var Ajedrez = (function () {

var _mostrar_tablero =  function (/*documento_xml*/){
  tablero = document.getElementById("tabla"); 
  for(f=0; f<8; f++) {
    var fila = tablero.insertRow(); 
    for(c=0; c<8; c++){
      var celda = fila.insertCell(); 
    }
  }
  var servidor = "https://enriqueztoraljuancarlos.bitbucket.io";
  var url = servidor + "/tablero.csv" ;
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (this.readyState === 4) {
      var mensaje = document.getElementById("mensaje");
      var cadena_de_texto="";
      mensaje.textContent = "";
      if (this.status === 200) {
        cadena_de_texto=this.response;
        if(cadena_de_texto.length==256){
                     
        filas = document.getElementById('tabla');
                        var filas;
                        var edo = 0;
                        var f = 0;
                        var c = 0;
                        var contador = 0;
                        console.log(cadena_de_texto);
                        var cadena=_limpia_cadena(cadena_de_texto);
                        for (f = 0; f <=7; f++) {
                            var fila = filas.insertRow();
                            for (c = 0; c <= 7; c++) {
                                var celda = fila.insertCell();
                                if (f % 2 == 0) {
                                    if (c % 2 == 0) {celda.innerHTML = '<span class=negras>' +_convierte_a_piezas(cadena[contador]) + '</span>';}
                                    else {celda.innerHTML = '<span class=blancas>' + _convierte_a_piezas(cadena[contador])+ '</span>';}
                                    contador++;
                                }
                                else {
                                    if (c % 2 == 0){celda.innerHTML = '<span class=blancas>' + _convierte_a_piezas(cadena[contador])+ '</span>';}
                                    else {celda.innerHTML = '<span class=negras>' + _convierte_a_piezas(cadena[contador]) + '</span>';}
                                    contador++;
                                }
                            }
                        }
                    }else{
                        mensaje.textContent = "Error al leer archivo";   
                    }
                } else {
                    mensaje.textContent = "Error " + this.status + " " + this.statusText
                        + " - " + this.responseURL;
                }
            }
        };
        xhr.open('GET', url, true);
        xhr.send();
    };
    /*funcion que recibe una cadena de caracteres y regresa una cadena sin " , y saltos de lina o { }*/
    var _limpia_cadena=function(cadena){
        var aux="";
        for(var i=0;i<cadena.length;i++){
            if (cadena[i] != '"' && cadena[i] != ',' && cadena[i] != '\n' && cadena[i] != '{' && cadena[i] != '}'){
                aux=aux+cadena[i];
            }
        }
        return aux;
    };
    /*convierte las letras lehidas en su simbolo en unicode*/
    var _convierte_a_piezas= function(letra){
        switch(letra){
            case "t": return "&#9820";
            case "c": return "&#9822";
            case "a": return "&#9821";
            case "r": return "&#9819";
            case "y": return "&#9818";
            case "p": return "&#9823";
            case "T": return "&#9814";
            case "C": return "&#9816";
            case "A": return "&#9815";
            case "R": return "&#9813";
            case "Y": return "&#9812";
            case "P": return "&#9817";
            default: return "&#934" ;


        }

};

var _actualizar_tablero = function() {
  var mensaje= document.getElementById("mensaje");      
  document.getElementById("tablero").innerHTML="<table id=\"tabla\"></table>";
  this.mostrarTablero();
};

var _mover_pieza = function(obj_parametros) {

  if (obj_parametros.a === undefined && obj_parametros.de === undefined && obj_parametros.a === SyntaxError && obj_parametros.de === SyntaxError){
                mensaje.textContent = "Error de parametros";
            }
            var de=obj_parametros.de;
            var a=obj_parametros.a;
            
            if(de[1]<9 && a[1]<9){
                var valor_de = document.getElementById("tabla").rows[_convertir_posicion(de)].cells[de[1] - 1].innerText;
                var valor_a  = document.getElementById("tabla").rows[_convertir_posicion(a)].cells[a[1] - 1].innerText;
                console.log(valor_de);
                console.log(valor_a);
                document.getElementById("tabla").rows[_convertir_posicion(de)].cells[de[1] - 1].innerHTML=valor_a;
                document.getElementById("tabla").rows[_convertir_posicion(a)].cells[a[1] - 1].innerHTML=valor_de;
                mensaje.textContent = "Echo";
            }else{
                mensaje.textContent="Error de posiciones";
            }
            
    };
    /*funcion para traducir los valores de entrada(caracter) a valor numerico en posicion del arreglo*/
    var _convertir_posicion = function(valor){
        var mensaje= document.getElementById("mensaje");
          switch(valor[0]){
              case 'a': return 0;
              case 'b': return 1;
              case 'c': return 2;
              case 'd': return 3;
              case 'e': return 4;
              case 'f': return 5;
              case 'g': return 6;
              case 'h': return 7;
              default: return mensaje.textContent="posicion no encontrada";
          }

};
return {
  "mostrarTablero": _mostrar_tablero,
  "actualizarTablero": _actualizar_tablero,
  "moverPieza": _mover_pieza
}
})();
